﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iText.Kernel.Pdf;
using iText.Kernel;
using iText.Kernel.Pdf.Canvas.Parser.Listener;
using iText.Kernel.Pdf.Canvas.Parser.Data;
using iText.Kernel.Pdf.Canvas.Parser;
using System.Xml;
using System.IO;

namespace ReadPDFStat
{
    class Program 
    {
        struct paramsofstart
        {
            public string incomingfilename;
            public string outcomingfilename;
            public Boolean producemerger;
            public double zeromove;
            public Boolean presentzeromove;
        }



        static paramsofstart correntparams = new paramsofstart();
        
        static int Main(string[] args)
        {
            int corrent = -1;
            correntparams.producemerger = false;
            for (int i = 0; i <= args.Length - 1; i++)
            {
                string str = args[i].ToString().Trim();
                string lstr = str.ToLower();
                switch (corrent)
                {
                    case -1:
                        if (lstr == "-incomingfilename")
                            corrent = 0;
                        else if (lstr == "-outcomingfilename")
                            corrent = 1;
                        else if (lstr == "-producemerger")
                            corrent = 2;
                        else if (lstr == "-zeromove")
                            corrent = 3;
                        else
                            corrent = -1;
                        break;
                    case 0:
                        correntparams.incomingfilename = str;
                        corrent = -1;
                        break;
                    case 1:
                        correntparams.outcomingfilename = str;
                        corrent = -1;
                        break;
                    case 2:
                        correntparams.producemerger = str=="1";
                        corrent = -1;
                        break;
                    case 3:
                        correntparams.presentzeromove = Double.TryParse(str, out correntparams.zeromove);
                        corrent = -1;
                        break;
                }
            }
            if (string.IsNullOrEmpty(correntparams.incomingfilename) | string.IsNullOrEmpty(correntparams.outcomingfilename))
            {
                Console.WriteLine("Incorrect params valid params:");
                Console.WriteLine("-incomingfilename");
                Console.WriteLine("-outcomingfilename");
                Console.WriteLine("-producemerger 0/1");
                return -1;
            }

            FileInfo Fi = new FileInfo(correntparams.incomingfilename);
            if (!Fi.Exists)
            {
                Console.WriteLine("File not found");
                return -1;
            }

            Fi = new FileInfo(correntparams.outcomingfilename);

            if (!Fi.Directory.Exists)
            {
                Console.WriteLine("Outcoming directory not found");
                return -1;
            }

            PdfReader reader = null;
            reader = new PdfReader(correntparams.incomingfilename);
            PdfDocument pdfDoc = new PdfDocument(reader);

            parser lparser = new parser();
            if(correntparams.presentzeromove == true)
            {
                lparser.zerrosdvig = correntparams.zeromove;
            }
            lparser.parcedocument(pdfDoc, correntparams.producemerger);
            lparser.saveresultusxml(correntparams.outcomingfilename);

            pdfDoc.Close();

            reader.Close();
            return 0;
        }

        

       
    }

    

    public class parser: IEventListener
    {
        protected struct pdfText
        {
            internal int page;
            internal double x;
            internal double y;
            internal string Text;
            public pdfText(int ppage, double px, double py, string ptext)
            {
                this.page = ppage;
                this.x = px;
                this.y = py;
                this.Text = ptext;
            }

            public override string ToString()
            {
                return page.ToString("G")+" "+ y.ToString("G9") + " " + x.ToString("G9") + " " + Text;
            }
        }

        protected class comperer : IComparer<pdfText>
        {
            public int Compare(pdfText x, pdfText y)
            {
                if ((x.page == y.page) & (x.y == y.y) & (x.x == y.x))
                    return 0;

                if (x.page > y.page)
                    return 1;
                else if (x.page < y.page)
                    return -1;

                else if (x.y > y.y)
                    return -1;
                else if (x.y < y.y)
                    return 1;

                else if (x.x > y.x)
                    return 1;
                else if (x.x < y.x)
                    return -1;

                else
                    return 0;
            }
        }

        protected List<pdfText> strings = new List<pdfText>();

        double formerpointx = 0;
        double currentpointx = 0;
        double formerpointy = 0;
        double currentpointy = 0;
        int currentpage;
        public double zerrosdvig = 0.0000000000001;



        public void parcedocument(PdfDocument pdfDoc,Boolean producemergepages)
        {
            for (int i = 1; i <= pdfDoc.GetNumberOfPages(); i++)
            {
                currentpage = i;
                PdfPage page = pdfDoc.GetPage(i);

                PdfCanvasProcessor processor = new PdfCanvasProcessor(this);
                processor.ProcessPageContent(page);
            }
            sort();
            if (producemergepages)
            {
                mergepages();
                sort();
            }
        }

        public int getsdvig<T>(List<T> a, List<T> b, Func<T, T, bool> match, out int pi, out int pj)
        {
            pi = -1;
            pj = -1;
            T[] tmp1 = new T[a.Count + b.Count - 1];
            T[] tmp2 = new T[a.Count + b.Count - 1];
            int[,] ressrav = new int[tmp1.Length - a.Count + 1, tmp2.Length - b.Count + 1];

            for (int i = 0; i <= tmp1.Length - a.Count; i++)
            {
                Array.Clear(tmp1, 0, tmp1.Length);
                a.CopyTo(tmp1, i);
                for (int j = tmp2.Length - b.Count; j >= 0; j--)
                {
                    Array.Clear(tmp2, 0, tmp2.Length);
                    b.CopyTo(tmp2, j);
                    int countequal = 0;
                    for (int k = Math.Max(i, j); k <= Math.Min(i + a.Count, j + b.Count) - 1; k++)
                    {
                        if (match.Invoke(tmp1[k], tmp2[k]))
                        {
                            countequal++;
                        }
                        else
                        {
                            countequal = 0;
                        }
                    }
                    ressrav[i, j] = countequal;
                }
            }

            int lmax = 0;
            for (int i = 0; i < ressrav.GetLength(0); i++)
                for (int j = 0; j < ressrav.GetLength(1); j++)
                    lmax = Math.Max(lmax, ressrav[i, j]);
            for (int i = 0; i < ressrav.GetLength(0); i++)
                for (int j = 0; j < ressrav.GetLength(1); j++)
                    if (lmax == ressrav[i, j])
                    {
                        pi = i;
                        pj = j;
                        return lmax;
                    }
            return -1;
        }
        private void sort()
        {
            strings.Sort(new comperer());
        }

        private string getlistofwords<T>(List<T> plist)
        {
            if (plist.Count == 0)
                return "";
            string lres = "";
            if(typeof(T)==typeof(int))
            {
                foreach (var i in plist)
                {
                    lres += "\r\n" + strings[(int)(object)(i)].Text;
                }
            }
            else if (typeof(T) == typeof(pdfText))
            {
                foreach (var i in plist)
                {
                    lres += "\r\n" + ((pdfText)(object)i).Text;
                }
            }
            return lres.Substring(2);
            
        }
        private void mergepages()
        {
            for (int i = 0; i < strings.Count;)
            {
                Console.Write('|');
                if (strings[i].page == 1) { i++; continue; };
                if (strings[i].page == 56)
                {
                    Console.WriteLine();
                }

                if (i == 6292)
                {
                    Console.WriteLine();
                }
                List<int> listofformer = new List<int>();
                List<int> listofcurrent = new List<int>();
                double maxX = 0;
                int MaxIndex = 0;
                int currentlastelement = getmaxindexlessthen(strings[i].page - 1, 10000, strings[i].y);
                while (currentlastelement != -1)
                {
                    maxX = Math.Max(maxX, strings[currentlastelement].x);
                    listofformer.Add(currentlastelement);
                    currentlastelement = getmaxindexlessthen(strings[i].page - 1, strings[currentlastelement].x, strings[i].y);
                }
                int currentfirstelement = getminindexmorethen(strings[i].page, 0, strings[i].y);
                while (currentfirstelement != -1)
                {
                    if (listofcurrent.Count == 0)
                    {
                        listofcurrent.Add(currentfirstelement);
                    }
                    else
                    {
                        listofcurrent.Insert(0, currentfirstelement);
                    }
                    MaxIndex = Math.Max(MaxIndex, currentfirstelement);
                    currentfirstelement = getminindexmorethen(strings[i].page, strings[currentfirstelement].x, strings[i].y);
                }

                List<int> indexesformerge = new List<int>();
                string tmp1 = getlistofwords<int>(listofformer);
                string tmp2 = getlistofwords<int>(listofcurrent);

                int li = -1;
                int lj = -1;
                int countequal = getsdvig<int>(listofformer, listofcurrent, (x, y) => strings[x].Text == strings[y].Text, out li, out lj);
                if (lj == 0)
                { 
                    for (int j = 0; j < countequal; j++)
                    {
                        indexesformerge.Add(listofformer[lj + j]);
                        indexesformerge.Add(listofcurrent[li + j]);                        
                    }
                }

                if (indexesformerge.Count != 0)
                {
                    for (int j = 1; j < indexesformerge.Count; j += 2)
                    {
                        strings.Insert(indexesformerge[j], new pdfText(0, strings[indexesformerge[j]].x + maxX, strings[indexesformerge[j]].y, strings[indexesformerge[j]].Text));
                        strings.RemoveAt(indexesformerge[j] + 1);
                        listofcurrent.Remove(indexesformerge[j]);
                    }
                    for (int j = 0; j < listofcurrent.Count; j++)
                    {
                        strings.Insert(listofcurrent[j], new pdfText(strings[listofcurrent[j]].page - 1, strings[listofcurrent[j]].x + maxX, strings[listofcurrent[j]].y, strings[listofcurrent[j]].Text));
                        strings.RemoveAt(listofcurrent[j] + 1);
                    }
                    
                }
                i = MaxIndex + 1;



            }
            for (int i = 0; i < strings.Count;)
            {
                if (strings[i].page == 0)
                {
                    strings.RemoveAt(i);
                    continue;
                }
                i++;
            }
        }

        private int getmaxindexlessthen(int ppage, double px, double py)
        {
            return strings.FindLastIndex(element => (element.page == ppage) & (element.y == py) & (element.x < px));
        }

        private int getminindexmorethen(int ppage, double px, double py)
        {
            return strings.FindIndex(element => (element.page == ppage) & (element.y == py) & (element.x > px));
        }

        public void saveresultusxml(string pathname)
        {

            XmlWriter lXMLwriter = XmlWriter.Create(pathname);
            lXMLwriter.WriteStartElement("Document");
            int formerpage = -1;
            int formerposy = -1;

            int formerposx = -1;

            foreach (pdfText element in strings)
            {
                if(formerpage != element.page)
                {
                    if (formerpage != -1)
                    {

                        lXMLwriter.WriteEndElement();//cell
                        lXMLwriter.WriteEndElement();//row
                        lXMLwriter.WriteEndElement();//page
                    }
                    lXMLwriter.WriteStartElement("page");
                    lXMLwriter.WriteAttributeString("position", element.page.ToString("G"));
                    formerposy = -1;
                    formerposx = -1;
                }

                if (formerposy != (int)Math.Round(element.y, 0))
                {
                    if (formerposy != -1)
                    { 
                        lXMLwriter.WriteEndElement();//cell
                        lXMLwriter.WriteEndElement();//row
                    }
                    lXMLwriter.WriteStartElement("row");
                    lXMLwriter.WriteAttributeString("position", element.y.ToString("G"));
                    formerposx = -1;
                }
                if (formerposx != (int)Math.Round(element.x, 0))
                {
                    if (formerposx != -1)
                        lXMLwriter.WriteEndElement();
                    lXMLwriter.WriteStartElement("Cell");
                    lXMLwriter.WriteAttributeString("position", element.x.ToString("G"));
                }

                lXMLwriter.WriteString(element.Text);

                formerpage = element.page;
                formerposy = (int)Math.Round(element.y,0);
                formerposx = (int)Math.Round(element.x,0);
            }

            //    lXMLwriter.WriteEndElement();
            lXMLwriter.WriteEndElement();//last cell
            lXMLwriter.WriteEndElement();//last row
            lXMLwriter.WriteEndElement();//last page
            lXMLwriter.WriteEndElement();//document

            lXMLwriter.Close();
        }
        public void EventOccurred(IEventData data, EventType type)
        {
            if (type == EventType.CLIP_PATH_CHANGED)
            {               
                currentpointx = ((iText.Kernel.Pdf.Canvas.Parser.Data.ClippingPathInfo)data).GetClippingPath().GetCurrentPoint().x;
                currentpointy = ((iText.Kernel.Pdf.Canvas.Parser.Data.ClippingPathInfo)data).GetClippingPath().GetCurrentPoint().y;
            }
            else if (type == EventType.RENDER_PATH)
            {
                if(data is PathRenderInfo)
                {
                    Console.WriteLine("pos {0} {1}", ((PathRenderInfo)data).GetPath().GetCurrentPoint().x.ToString(), ((PathRenderInfo)data).GetPath().GetCurrentPoint().y.ToString());
                }
            }
            else if (type == EventType.RENDER_TEXT)
            {
                if (data is AbstractRenderInfo)
                {
                    
                    ((AbstractRenderInfo)data).PreserveGraphicsState();
                    if (formerpointy == currentpointy)
                    {
                        Console.Write("|");
                    }
                    else
                    {
                        Console.Write("\r\n");
                    }
                    string ltext = ((iText.Kernel.Pdf.Canvas.Parser.Data.TextRenderInfo)data).GetText();
                    if (!String.IsNullOrEmpty(ltext))
                    {
                        Console.Write(ltext);
                    }
                    this.strings.Add(new pdfText(this.currentpage, currentpointx, currentpointy, ltext));
                    if(ltext== "ATC ticket")
                    {
                        Console.WriteLine();
                    }
                    currentpointx = currentpointx + zerrosdvig;
                    formerpointx = currentpointx;
                    formerpointy = currentpointy;
                }
            }
            else
            {
                Console.WriteLine(type.ToString());
            }
        }

        public ICollection<EventType> GetSupportedEvents()
        {
            return null;
        }

        public parser()
        {
        }
    }


}
